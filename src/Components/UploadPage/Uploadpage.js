import React from 'react'
import StatusModels from '../../Models/StatusModels'
import {
    UploadPageContainer,
    UploadCenterContainer,
    UploadPageForm,
    UploadPageFormLabel,
    UploadPageFormInp,
    UploadStatusNav,
    UploadStatusbutton,
    UploadStatusUl,
    UploadStatusLi,
    UploadStatusButtonStyle,
    DownIcon
} from './UploadpageElements'
const Uploadpage = () => {
    const [isOpen, setIsOpen] = React.useState(false)
    return (
        <UploadPageContainer className="show-text" >
            <UploadCenterContainer>
                <UploadPageForm>
                    <UploadPageFormLabel>File Name *</UploadPageFormLabel>
                    <UploadPageFormInp />
                </UploadPageForm>
                <UploadPageForm>
                    <UploadPageFormLabel>Version *</UploadPageFormLabel>
                    <UploadPageFormInp />
                </UploadPageForm>
                <UploadPageForm>
                    <UploadPageFormLabel>สถานะการใช้งาน *</UploadPageFormLabel>
                    <UploadStatusNav>
                        <UploadStatusButtonStyle onClick={() => setIsOpen(!isOpen)} >
                            <DownIcon />
                            <UploadStatusbutton>--กรุณาเลือก--</UploadStatusbutton>
                        </UploadStatusButtonStyle>

                        {isOpen && <UploadStatusUl className="fade" >
                            {StatusModels.map((res, i) => {
                                return <UploadStatusLi onClick={() => setIsOpen(false)}>
                                    {res.statusName}
                                </UploadStatusLi>

                            })}
                        </UploadStatusUl>}

                    </UploadStatusNav>

                </UploadPageForm>
            </UploadCenterContainer>
        </UploadPageContainer>
    )
}

export default Uploadpage
