import React from 'react'
import moment from 'moment';
import {
    PaginateSearch,
    PaginationSearchForm,
    PaginationSearchInp,
    BsSearchIc
} from '../TemplateList/TemplateListElements'
import { UserGroupContainer, PageName, PageNameH1, PageButtonContainer, PageButtonRefresh, PageButtonCreate, PageButtonClose, ButtAdd, ButtRef } from "../ConsiderUserGroup/UserGroupElements";

import UploadPageList from './UploadPageList';
import { useHistory } from 'react-router-dom';

function createData(id, filename, version, status, createdate) {
    return { id, filename, version, status, createdate };
}

const rows = [
    createData('1', "NOM_data_V.0.0.1", "V.2.0.1", "ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('2', "NOM_data_V.0.0.1", "V.2.0.1", "ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('3', "NOM_data_V.0.0.1", "V.0.02.1", "ไม่ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('4', "NOM_data_V.0.0.1", "V.0.0.2", "ไม่ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('5', "NOM_data_V.0.0.1", "V..0.1", "ใช้งาน", moment().format('MMMM Do YYYY')),
];
const UploadpageListSearch = () => {
    const history = useHistory()
    const [q, setQ] = React.useState("");
    function search(rows) {
        const columns = rows[0] && Object.keys(rows[0]);
        return rows.filter((row) =>
            columns.some(
                (column) =>
                    row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
            )
        );
    }
    return (
        <div>
            <PageName>
                <PageNameH1>Portal Datas</PageNameH1>
                <PageButtonContainer className="pagebutt-grap2" >
                    <PageButtonRefresh>
                        <ButtRef />
                        รีเฟรช
                    </PageButtonRefresh>
                    <PageButtonCreate onClick={()=>{history.push("/uploads-files")}} >
                        <ButtAdd />
                        เพิ่ม
                    </PageButtonCreate>
                </PageButtonContainer>
            </PageName>
            <PaginateSearch>
                <PaginationSearchForm>
                    <BsSearchIc />
                    <PaginationSearchInp
                        type="search"
                        value={q}
                        onChange={(e) => setQ(e.target.value)} />
                </PaginationSearchForm>
            </PaginateSearch>
            <UploadPageList data={search(rows)} />
        </div>
    )
}

export default UploadpageListSearch
