import styled from 'styled-components'
import { AiOutlineCaretDown } from 'react-icons/ai'

export const UploadPageContainer = styled.div`
  
  &.show-text {
    position: relative;
  }

`
export const UploadCenterContainer = styled.div`
  width: 100%;
  box-shadow: 0px 7px 13px rgba(0, 0, 0, 0.25);
  height: 30vh;
  background: #fff;
  padding-top: 1rem;
  padding-bottom: 1rem;
  
`
export const UploadPageForm = styled.div`
    display: grid;
    /* grid-column-gap: 1rem; */
    grid-template-columns: repeat(2,.3fr);
    justify-content: center;
    margin-top: 1rem;
`
export const UploadPageFormLabel = styled.p`
    text-align: right;
    margin-right: 1rem;
    
`
export const UploadPageFormInp = styled.input`
    width: 100%;
    border-radius: 0rem;
    background: #fff;
    outline: none;
    border: none;
    border-bottom: 1px solid #000;
    &:hover{
        background: #fff;
    }
`
export const UploadStatusButtonStyle = styled.div``
export const UploadStatusNav = styled.nav``
export const UploadStatusbutton = styled.button`
    width: 100%;
    border-bottom: 1px solid #000;
    border-radius: 0rem;
    background-color: #fff;
    
    &:hover{
        background: #fff;
    }
`
export const UploadStatusUl = styled.ul`
    border: 1px solid #000;
    box-shadow: 0px 7px 13px rgba(0, 0, 0, 0.25);
    

`
export const UploadStatusLi = styled.li`
    padding: 1rem 0 1rem 1rem;
`
export const DownIcon = styled(AiOutlineCaretDown)`
    float: right;
    position: absolute;
    right: 20%;
    margin-top: .5rem;
`