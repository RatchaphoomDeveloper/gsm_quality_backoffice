import React from "react";
import { Switch, Link, Route } from "react-router-dom";
import Calendar from "../Calendar/Calendar";
import Editor from "../Editor/Editor";
import Log from "../Log/Log";
import Main from "../Main/Main";
import Template from "../Template/Template";
import ConsiderUser from "../UserPermission/ConsiderUser";
import { AiOutlineFileWord } from "react-icons/ai";
import "./SideBar.scss";
import TemplateSearch from "../TemplateList/TemplateSearch";
import { FaReact } from "react-icons/fa";
import UploadpageListSearch from "../UploadPage/UploadpageListSearch";
import UserGroupForm from "../ConsiderUserGroup/UserGroupForm";
import Uploadpage from "../UploadPage/Uploadpage";
const SideBar = () => {
  return (
    <div>
      <div className="sidebar">
        <div className="sidebar-header">
          <h2>GSM LOGISTICS </h2>
          <FaReact />
          <span className="fa fa-bars"></span>
        </div>
        <div className="sidebar-menu">
          <div className="">
            <ul>
              <li>
                <Link to={"/"}>
                  <span className="fas fa-home"></span>
                  <span>Home</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div>
          <div className="detail-side">
            <p id="menu-p">ข้อมูล</p>
            <ul>
              <li>
                <Link to={"/login_log"}>
                  <span className="fas fa-wrench"></span>
                  <span>ประวัติการเข้าสู่ระบบ</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div>
          <div className="foot-side">
            <p id="menu-p">การจัดการ</p>
            <ul>
              <li>
                <Link to={"/create_calendar"}>
                  <span className="fas fa-cog"></span>
                  <span>สร้าง PM Plan</span>
                </Link>
              </li>
              <li>
                <Link to={"/create_template"}>
                  <span className="fas fa-cog"></span>
                  <span>สร้างเทมเพลต</span>
                </Link>
              </li>
              <li>
                <Link to={"/template-list"}>
                  <span className="fas fa-cog"></span>
                  <span>รายการเทมเพลต</span>
                </Link>
              </li>
              <li>
                <Link to={"/consider_user_group"}>
                  <span className="fas fa-cog"></span>
                  <span>พิจรณาสิทธิ์ผู้ใช้งานระบบ </span>
                </Link>
              </li>
              <li>
                <Link to={"/consider_user_group"}>
                  <span className="fas fa-cog"></span>
                  <span>ผู้มีสิทธิ์ใช้งานระบบ </span>
                </Link>
              </li>
              <li>
                <Link to={"/consider_user_group"}>
                  <span className="fas fa-cog"></span>
                  <span>กำหนดกลุ่มผู้ใช้งาน </span>
                </Link>
              </li>
              <li>
                <Link to={"/consider_user"}>
                  <span className="fas fa-cog"></span>
                  <span>กำหนดสิทธิ์การใช้งาน </span>
                </Link>
              </li>
              <li>
                <Link to={"/consider_user"}>
                  <span className="fas fa-cog"></span>
                  <span>เปลี่ยนรหัสผ่าน </span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
            <div className="detail-side">
              <p id="menu-p">จัดการไฟล์</p>
              <ul>
                <li>
                  <Link to={"/upload-filelist"}>
                    <AiOutlineFileWord />
                    <span>Upload Files</span>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="main-side"></div>
            <div className="detail-side">
              <p
                id="menu-p-main"
                onClick={() => {
                  window.location.href =
                    "https://pttwebtest9.pttplc.com/PTT-GSM-Logistics_Test/login?returnUrl=https:%2F%2Fpttwebtest9.pttplc.com%2FPTT-GSM-Logistics_Test";
                }}
              >
                PTT GAS Nomination
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="main-content">
        <header>
          <div className="search-wrapper">
            <span className="fas fa-search"></span>
            <input type="search" placeholder="ค้นหา" />
          </div>
          <div className="social-icons">
            <span className="far fa-bell"></span>
            <span className="far fa-comment-alt"></span>
            <span className="fas fa-user-circle"></span>
          </div>
        </header>
        <main>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/login_log" component={Log} />
            <Route exact path="/create_template" component={Template} />
            <Route exact path="/template-list" component={TemplateSearch} />
            <Route exact path="/create_editor_page" component={Editor} />
            <Route exact path="/consider_user" component={ConsiderUser} />
            <Route exact path="/create_calendar" component={Calendar} />
            <Route
              exact
              path="/upload-filelist"
              component={UploadpageListSearch}
            />
            <Route exact path="/uploads-files" component={Uploadpage} />
            <Route exact path="/consider_user_group" component={UserGroupForm} />
          </Switch>
        </main>
      </div>
    </div>
  );
};

export default SideBar;
