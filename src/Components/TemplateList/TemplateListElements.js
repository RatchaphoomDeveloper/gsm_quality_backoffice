import styled from 'styled-components'
import {BsSearch} from 'react-icons/bs'

export const EditStyledCon  = styled.div`
    cursor: pointer;
    &:hover{
        
    }
`

export const StatusContainerP = styled.p`
    font-weight: 800;
    &.active{
        color:#228B22 !important
    }
    &.deactive{
        color:#FF0000 !important
    }
`
export const TemplateListContainer = styled.div`

`
export const PaginationContainer = styled.div`
    margin-top: 1rem;
`

export const PaginateSearch = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: center;
`

export const PaginationSearchForm = styled.div`
    
`
export const PaginationSearchInp = styled.input`
    border: none;
    outline: none;
    margin-bottom: 1rem;
    padding-left: 2rem;
`

export const BsSearchIc = styled(BsSearch)`
    position: absolute;
    margin-top: .5rem;
    margin-left: .5rem;
`