import React from 'react'
import TemplateList from './TemplateList';
import moment from 'moment';
import {
    PaginateSearch,
    PaginationSearchForm,
    PaginationSearchInp,
    BsSearchIc
} from './TemplateListElements'

function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}

const rows = [
    createData('1', "Home", "/home", "ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('2', "About-us", "/about-us", "ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('3', "News", "/news", "ไม่ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('4', "Contact-us", "/contact-us", "ไม่ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('5', "Graph", "/graph", "ใช้งาน", moment().format('MMMM Do YYYY')),
];
const TemplateSearch = () => {
    const [q, setQ] = React.useState("");
    function search(rows) {
        const columns = rows[0] && Object.keys(rows[0]);
        return rows.filter((row) =>
            columns.some(
                (column) =>
                    row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
            )
        );
    }
    return (
        <div>
            <PaginateSearch>
                <PaginationSearchForm>
                    <BsSearchIc />
                    <PaginationSearchInp
                        type="search"
                        value={q}
                        onChange={(e) => setQ(e.target.value)} />
                </PaginationSearchForm>
            </PaginateSearch>
            <TemplateList data={search(rows)} />
        </div>
    )
}

export default TemplateSearch
