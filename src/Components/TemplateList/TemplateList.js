import React from 'react'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { BiEdit } from 'react-icons/bi'
import {
    EditStyledCon,
    StatusContainerP,
    TemplateListContainer,
    PaginationContainer,
} from './TemplateListElements'
import ReactPaginate from "react-paginate";
import '../../Utility/dataTable/index.scss'
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#0b7bb3",
        color: theme.palette.common.white,
        textAlign: "center",
        border: "1px solid #fff",
        fontWeight: "bold"
    },
    body: {
        fontSize: "16px",
        textAlign: "center",
        border: "1px solid #fff",

    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
});


const TemplateList = ({data}) => {
    const classes = useStyles();
    const [pageNumber, setPageNumber] = React.useState(0);
    const dataPerPage = 5;
    const pagesVisited = pageNumber * dataPerPage;
    const pageCount = Math.ceil(data.length / dataPerPage);
    const changePage = ({ selected }) => {
        setPageNumber(selected);
    };
    const disPlayData = data
        .slice(pagesVisited, pagesVisited + dataPerPage)
        .map((res, i) => {
            return <StyledTableRow key={res.name} >
                <StyledTableCell component="th" scope="row">
                    {res.name}
                </StyledTableCell>
                <StyledTableCell align="right">{res.calories}</StyledTableCell>
                <StyledTableCell align="right">{res.fat}</StyledTableCell>
                <StyledTableCell align="right">
                    <StatusContainerP className={res.carbs === "ใช้งาน" ? "active" : "deactive"}  >
                        {res.carbs}
                    </StatusContainerP>
                </StyledTableCell>
                <StyledTableCell align="right">{res.protein}</StyledTableCell>
                <StyledTableCell align="center">
                    <EditStyledCon>
                        <BiEdit />
                    </EditStyledCon>
                </StyledTableCell>
            </StyledTableRow>
        })
 
    return (
        <TemplateListContainer>
          
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>ID</StyledTableCell>
                            <StyledTableCell align="right">ชื่อเพลต</StyledTableCell>
                            <StyledTableCell align="right">ชื่อพาทย์</StyledTableCell>
                            <StyledTableCell align="right">สถานะ</StyledTableCell>
                            <StyledTableCell align="right">Create Date</StyledTableCell>
                            <StyledTableCell align="right"></StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody >
                        {disPlayData}
                    </TableBody>
                </Table>
            </TableContainer>
            <PaginationContainer>
                <ReactPaginate
                    previousLabel={"Prev"}
                    nextLabel={"Next"}
                    pageCount={pageCount}
                    onPageChange={changePage}
                    containerClassName={"paginationBttns"}
                    previousLinkClassName={"previousBttn"}
                    nextLinkClassName={"nextBttn"}
                    disabledClassName={"paginationDisabled"}
                    activeClassName={"paginationActive"}
                />
            </PaginationContainer>

        </TemplateListContainer>

    );
}

export default TemplateList
