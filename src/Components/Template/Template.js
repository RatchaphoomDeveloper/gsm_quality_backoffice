import React from "react";
import './Template.scss'
const Template = () => {

  return (
    <div>
      <div className="title-container">
        <h2 className="dash-tittle">สร้างเทมเพลต</h2>
      </div>
      <div className="logic-container" >
          <div className="create-template-container" >
            <div className="create-template-form-group" >
              <label>Page name</label>
              <input type="text" />
            </div>
            <div className="create-template-form-group" >
              <label>Router name</label>
              <input type="text" />
            </div>
            <button onClick={()=>{window.open("https://builder.io/content")}} >Open Editor</button>
          </div>
      </div>
      
    </div>
  );
};

export default Template;
