import styled from "styled-components";
import { FiRefreshCcw } from 'react-icons/fi'
import { RiAddCircleFill } from 'react-icons/ri'
export const UserGroupContainer = styled.div``;
export const UserGroupShowFormContainer = styled.div`
  width: 100%;
  box-shadow: 0px 7px 13px rgba(0, 0, 0, 0.25);
  height: 100%;
  background: #fff;
  &.show-text {
    position: relative;
    text-align: center;
  }
`;

export const ShowTitle = styled.p`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;

export const GroupForm = styled.div``;
export const GroupInpForm = styled.div`
  padding-left: 1rem;
  padding-right: 1rem;
  padding-top: 1rem;
`;

export const GroupInpLabel = styled.p``;

export const GroupInp = styled.input`
  border: none;
  outline: none;
  border-bottom: 1px solid #000;
  border-radius: 0rem;
`;

export const GroupRadioForm = styled.div`
  padding-left: 1rem;
  padding-right: 1rem;
  padding-top: 1rem;
  /* display: grid;
  grid-template-columns: repeat(2,1fr); */
`;

export const GroupRadioFormUl = styled.div`
  display: flex;
  align-items: center;
  padding-left: 1rem;
  padding-right: 1rem;
  padding-top: 1rem;
`;

export const GroupRadioli = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
export const GroupRadio = styled.input`
  width: fit-content;
  margin-right: 1rem;
  &.radi-right {
    margin-left: 1rem;
  }
`;
// GroupRadioForm,
//   GroupRadioli,
//   GroupRadio,
export const GroupFormDateAndUser = styled.div``
export const GroupFormDateAndUserUl = styled.ul`
  display: grid;
  grid-template-columns: repeat(2,1fr);
  grid-column-gap: 1rem;
  padding: 1rem;
`
export const GroupFormDateAndUserLi = styled.li`
  padding-bottom: 1rem;
`;
export const GroupFormDateAndUserLabel = styled.p``
export const GroupFormDateAndUserInp = styled.input`
  border: none;
  outline: none;
  border-bottom: 1px solid #000;
  border-radius: 0rem;
`;

export const PageName = styled.div`
  width: 98%;
  box-shadow: 0px 7px 13px rgba(0, 0, 0, 0.25);
  height: 50%;
  padding: 1rem;
  margin-bottom: 1rem;
  background: #fff;
  border-radius: 5px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  
`;
export const PageNameH1 = styled.h2``

export const PageButtonContainer = styled.div`
  display: grid;
  grid-column-gap: 1rem;
  grid-template-columns: repeat(3,1fr);
  &.pagebutt-grap2{
    display: grid;
    grid-column-gap: 1rem;
    grid-template-columns: repeat(2,1fr);
  }
`
export const PageButtonRefresh = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
`
export const PageButtonCreate = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
`
export const PageButtonClose = styled.button``

export const ButtRef = styled(FiRefreshCcw)`
  color: black;
  padding-right: 0.5rem;
`
export const ButtAdd = styled(RiAddCircleFill)`
  color: black;
  font-size: 18px;
  padding-right: 0.5rem;
`