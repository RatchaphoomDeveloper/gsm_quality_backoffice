import React from "react";
import { Container, Grid } from "@material-ui/core";
import UserGroupSearch from "./UserGroupSearch";
import UserGroupShowForm from "./UserGroupShowForm"
import { UserGroupContainer, PageName, PageNameH1, PageButtonContainer, PageButtonRefresh, PageButtonCreate, PageButtonClose,ButtAdd,ButtRef } from "./UserGroupElements";
const UserGroupForm = () => {
  const [groupId, setGroupId] = React.useState(1)
  return (
    <UserGroupContainer>
      <PageName>
        <PageNameH1>Managment/กำหนดกลุ่มผู้ใช้งาน</PageNameH1>
        <PageButtonContainer>
          <PageButtonRefresh>
            <ButtRef/>
            รีเฟรช
          </PageButtonRefresh>
          <PageButtonCreate>
            <ButtAdd/>
            เพิ่ม
          </PageButtonCreate>
          <PageButtonClose>
            ปิด
          </PageButtonClose>
        </PageButtonContainer>
      </PageName>

      <Grid container spacing={3}>
        <Grid item xs={6}>
          <UserGroupSearch setGroupId={setGroupId} />
        </Grid>
        <Grid item xs={6}>
          <UserGroupShowForm groupId={groupId} />
        </Grid>
      </Grid>
    </UserGroupContainer>
  );
};

export default UserGroupForm;
