import React from "react";
import moment from "moment";
import {
  PaginateSearch,
  PaginationSearchForm,
  PaginationSearchInp,
  BsSearchIc,
} from "../TemplateList/TemplateListElements";
import UserGroupList from "./UserGroupList";
function createData(id, groupname, message, status) {
  return { id, groupname, message, status };
}

const rows = [
  createData("1", "expert", "", "ใช้งาน"),
  createData("2", "operator", "", "ใช้งาน"),
  createData("3", "admin", "", "ไม่ใช้งาน"),
  createData("4", "supervisor", "", "ไม่ใช้งาน"),
  createData("5", "Graph", "", "ใช้งาน"),
];
const UserGroupSearch = ({ setGroupId }) => {
  const [q, setQ] = React.useState("");
  function search(rows) {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter((row) =>
      columns.some(
        (column) =>
          row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
      )
    );
  }
  return (
    <div>
      <PaginateSearch>
        <PaginationSearchForm>
          <BsSearchIc />
          <PaginationSearchInp
            type="search"
            value={q}
            onChange={(e) => setQ(e.target.value)}
          />
        </PaginationSearchForm>
      </PaginateSearch>
      <UserGroupList data={search(rows)} setGroupId={setGroupId} />
    </div>
  );
};

export default UserGroupSearch;
