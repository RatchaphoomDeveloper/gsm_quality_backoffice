import React from "react";
import moment from "moment";
import {
  UserGroupShowFormContainer,
  ShowTitle,
  GroupForm,
  GroupInpForm,
  GroupInpLabel,
  GroupInp,
  GroupRadioForm,
  GroupRadioli,
  GroupRadio,
  GroupRadioFormUl,
  GroupFormDateAndUser,
  GroupFormDateAndUserUl,
  GroupFormDateAndUserLabel,
  GroupFormDateAndUserInp,
  GroupFormDateAndUserLi,
} from "./UserGroupElements";

function createData(id, groupname, message, status) {
  return { id, groupname, message, status };
}

const rows = [
  createData("1", "expert", "", "ใช้งาน"),
  createData("2", "operator", "", "ใช้งาน"),
  createData("3", "admin", "", "ไม่ใช้งาน"),
  createData("4", "supervisor", "", "ไม่ใช้งาน"),
  createData("5", "Graph", "", "ใช้งาน"),
];

const UserGroupShowForm = ({ groupId }) => {
  const [userGroup, setUserGroup] = React.useState([]);
  const [status, setStatus] = React.useState("");
  React.useEffect(() => {
    filterGroup();
  }, [groupId]);
  const filterGroup = () => {
    if (groupId !== null) {
      let groupFilterData = rows && rows.filter((res, i) => res.id === groupId);
        if (groupFilterData[0]) {
        setUserGroup(groupFilterData[0]);
        setStatus(groupFilterData[0] && groupFilterData[0].status);
      }
    }
  };
  return (
    <UserGroupShowFormContainer className={groupId === null ? "show-text" : ""}>
      {groupId === null ? (
        <ShowTitle>กดเลือกข้อมูลที่ต้องการแก้ไข</ShowTitle>
      ) : (
        <GroupForm>
          <GroupInpForm>
            <GroupInpLabel>ชื่อกลุ่ม:</GroupInpLabel>
            <GroupInp value={userGroup.groupname} />
          </GroupInpForm>
          <GroupInpForm>
            <GroupInpLabel>หมายเหตุ:</GroupInpLabel>
            <GroupInp value={userGroup.message} />
          </GroupInpForm>
          <GroupRadioForm>
            <GroupInpLabel>สถานะ:</GroupInpLabel>
            <GroupRadioFormUl>
              <GroupRadioli>
                <GroupRadio
                  type={"radio"}
                  name="status"
                  value="ใช้งาน"
                  checked={status === "ใช้งาน" && true}
                  onChange={(e) => setStatus(e.target.value)}
                />
                <p>ใช้งาน</p>
              </GroupRadioli>
              <GroupRadioli>
                <GroupRadio
                  className="radi-right"
                  type={"radio"}
                  name="status"
                  value="ไม่ใช้งาน"
                  checked={status === "ไม่ใช้งาน" && true}
                  onChange={(e) => setStatus(e.target.value)}
                />
                <p>ไม่ใช้งาน</p>
              </GroupRadioli>
            </GroupRadioFormUl>
          </GroupRadioForm>
          <GroupFormDateAndUser>
            <GroupFormDateAndUserUl>
              <GroupFormDateAndUserLi>
                <GroupFormDateAndUserLabel>Create on</GroupFormDateAndUserLabel>
                <GroupFormDateAndUserInp
                  type="text"
                  value={moment().format("DD/MM/YYYY HH:mm:ss")}
                  disabled
                />
              </GroupFormDateAndUserLi>
              <GroupFormDateAndUserLi>
                <GroupFormDateAndUserLabel>Create by</GroupFormDateAndUserLabel>
                <GroupFormDateAndUserInp
                  type="text"
                  value="zratchaphoom.b"
                  disabled
                />
              </GroupFormDateAndUserLi>
              <GroupFormDateAndUserLi>
                <GroupFormDateAndUserLabel>Update on</GroupFormDateAndUserLabel>
                <GroupFormDateAndUserInp
                  type="text"
                  type="text"
                  value={moment().format("DD/MM/YYYY HH:mm:ss")}
                  disabled
                />
              </GroupFormDateAndUserLi>
              <GroupFormDateAndUserLi>
                <GroupFormDateAndUserLabel>Update by</GroupFormDateAndUserLabel>
                <GroupFormDateAndUserInp
                  type="text"
                  value="issara.s"
                  disabled
                />
              </GroupFormDateAndUserLi>
            </GroupFormDateAndUserUl>
          </GroupFormDateAndUser>
        </GroupForm>
      )}
    </UserGroupShowFormContainer>
  );
};

export default UserGroupShowForm;
