import React from "react";
import { NavLink } from "react-router-dom";
import ColumnChart from "../../Utility/GraphView/column charts/Column Chart";
import LineChart from '../../Utility/GraphView/line charts/Line Chart'
import SplineChart from '../../Utility/GraphView/line charts/Spline Chart'
import StockChartNumeric from '../../Utility/GraphView/stock charts/StockChart with Numeric Axis'
import "./Main.scss";
const Main = () => {
  return (
    <div>
      <h2 className="dash-tittle">
        {window.location.pathname === "/" && "Home"}
      </h2>
      <div className="dash-cards">
        <div className="card-single">
          <div className="card-body">
            <span className="fas fa-briefcase"></span>
            <div>
              <h5>EARNINGS(MONNTHLY)</h5>
              <h4>$30,659.44</h4>
            </div>
          </div>
          <div className="card-footer">
            <NavLink to="/">View all</NavLink>
          </div>
        </div>

        <div className="card-single">
          <div className="card-body">
            <span className="fas fa-briefcase"></span>
            <div>
              <h5>EARNINGS(ANNUAL)</h5>
              <h4>$30,659.44</h4>
            </div>
          </div>
          <div className="card-footer">
            <NavLink to="/">View all</NavLink>
          </div>
        </div>

        <div className="card-single">
          <div className="card-body">
            <span className="fa fa-th-list"></span>
            <div>
              <h5>TASKS</h5>
              <h4>50%</h4>
            </div>
          </div>
          <div className="card-footer">
            <NavLink to="/">View all</NavLink>
          </div>
        </div>

        <div className="card-single">
          <div className="card-body">
            <span className="fas fa-sync"></span>
            <div>
              <h5>PENDING REQUESTS</h5>
              <h4>18</h4>
            </div>
          </div>
          <div className="card-footer">
            <NavLink to="/">View all</NavLink>
          </div>
        </div>
      </div>
      <div className="dash-cards">
        <div className="card-single">
          <LineChart/>
        </div>
        <div className="card-single">
          <SplineChart/>
        </div>
      </div>
      <div className="card-full-width">
        <StockChartNumeric/>
      </div>
      <div className="card-full-width">
        <ColumnChart />
      </div>
    </div>
  );
};

export default Main;
