import React from 'react'
import {
    PaginateSearch,
    PaginationSearchForm,
    PaginationSearchInp,
    BsSearchIc
} from '../TemplateList/TemplateListElements'
import moment from 'moment';
import ConsiderList from './ConsiderList';

function createData(id,name, username, status, createdate) {
    return { id,name, username, status, createdate };
}

const rows = [
    createData('1', "zratchaphoom.b", "รัชชภูมิ บุนนาค", "ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('2', "issara.s", "Issara Satawiriya", "ใช้งาน",moment().format('MMMM Do YYYY')),
    createData('3', "zsupachai.p", "Supachai Phanpermpoon", "ไม่ใช้งาน", moment().format('MMMM Do YYYY')),
    createData('4', "zphurithat.k", "Phurithat Khajornjit", "ไม่ใช้งาน", moment().format('MMMM Do YYYY')),
];
const ConsiderListSearch = () => {
    const [q, setQ] = React.useState("");
    function search(rows) {
        const columns = rows[0] && Object.keys(rows[0]);
        return rows.filter((row) =>
            columns.some(
                (column) =>
                    row[column].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
            )
        );
    }
    return (
        <div>
              <PaginateSearch>
                <PaginationSearchForm>
                    <BsSearchIc />
                    <PaginationSearchInp
                        type="search"
                        value={q}
                        onChange={(e) => setQ(e.target.value)} />
                </PaginationSearchForm>
            </PaginateSearch>
            <ConsiderList data={search(rows)} />
        </div>
    )
}

export default ConsiderListSearch
