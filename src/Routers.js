import React from 'react'
import { HashRouter,Switch,Route } from "react-router-dom";
import SideBar from './Components/SideBar/SideBar'
const Routers = () => {
    return (
        <div>
            <HashRouter basename="/" >
                <SideBar/>
            </HashRouter>
        </div>
    )
}

export default Routers
